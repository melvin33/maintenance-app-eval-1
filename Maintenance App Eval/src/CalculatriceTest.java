import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CalculatriceTest {

	@Test
	void testAdditionner() {
		Calculatrice calculatrice = new Calculatrice();
		assertEquals(2, calculatrice.additionner(1, 1));
	}

}
